"""
CS6476 Problem Set 5 imports. Only Numpy and cv2 are allowed.
"""
import numpy as np
import cv2


# Assignment code
# Leveraged Code from my AI4R project
class KalmanFilter(object):
    """A Kalman filter tracker"""

    def __init__(self, init_x, init_y, Q=0.1 * np.eye(4), R=0.1 * np.eye(2)):
        """Initializes the Kalman Filter

        Args:
            init_x (int or float): Initial x position.
            init_y (int or float): Initial y position.
            Q (numpy.array): Process noise array.
            R (numpy.array): Measurement noise array.
        """
        # State vector
        self.state = np.array([init_x, init_y, 0., 0.])

        # Initial uncertainty
        # MDT twiddle these values
        self.P = np.array([[0.001, 0., 0., 0.],
                           [0., 0.001, 0., 0.],
                           [0., 0., 1000., 0.],
                           [0., 0., 0., 1000.]])

        # Dynamic Model Transition (Next State Function)
        # x_t = x_t-1 + v_x * dt
        # y_t = y_t-1 + v_y * dt
        self.D = np.array([[1., 0., 1., 0.],
                           [0., 1., 0., 1.],
                           [0., 0., 1., 0.],
                           [0., 0., 0., 1.]])

        # Process Noise - Sigma_dt
        self.Q = Q

        # Measurement Function
        # Reflect the fact that we observe x and y but not the two velocities
        self.M = np.array([[1., 0., 0., 0.],
                           [0., 1., 0., 0.]])

        # Measurement Noise - Sigma_mt
        self.R = R

        self.I = np.eye(4)

    def predict(self):
        # X_t = D_t * X_t-1
        self.state = np.dot(self.D, self.state)
        # Sigma_t = D_t * Sigma_t-1 * D_t.T + Sigma_dt
        self.P = np.linalg.multi_dot([self.D, self.P, self.D.T]) + self.Q

    def correct(self, meas_x, meas_y):
        # Kalman Gain
        S = np.linalg.multi_dot([self.M, self.P, self.M.T]) + self.R
        K = np.linalg.multi_dot([self.P, self.M.T, np.linalg.inv(S)])

        # Measurement update
        Y = np.array([meas_x, meas_y])
        residual_error = Y - np.dot(self.M, self.state)
        self.state = self.state + np.dot(K, residual_error)
        self.P = np.dot((self.I - np.dot(K, self.M)), self.P)

    def process(self, measurement_x, measurement_y):

        self.predict()
        self.correct(measurement_x, measurement_y)

        return self.state[0], self.state[1]


class ParticleFilter(object):
    """A particle filter tracker.

    Encapsulating state, initialization and update methods. Refer to
    the method run_particle_filter( ) in experiment.py to understand
    how this class and methods work.
    """

    def __init__(self, frame, template, **kwargs):
        """Initializes the particle filter object.

        The main components of your particle filter should at least be:
        - self.particles (numpy.array): Here you will store your particles.
                                        This should be a N x 2 array where
                                        N = self.num_particles. This component
                                        is used by the autograder so make sure
                                        you define it appropriately.
                                        Make sure you use (x, y)
        - self.weights (numpy.array): Array of N weights, one for each
                                      particle.
                                      Hint: initialize them with a uniform
                                      normalized distribution (equal weight for
                                      each one). Required by the autograder.
        - self.template (numpy.array): Cropped section of the first video
                                       frame that will be used as the template
                                       to track.
        - self.frame (numpy.array): Current image frame.

        Args:
            frame (numpy.array): color BGR uint8 image of initial video frame,
                                 values in [0, 255].
            template (numpy.array): color BGR uint8 image of patch to track,
                                    values in [0, 255].
            kwargs: keyword arguments needed by particle filter model:
                    - num_particles (int): number of particles.
                    - sigma_exp (float): sigma value used in the similarity
                                         measure.
                    - sigma_dyn (float): sigma value that can be used when
                                         adding gaussian noise to u and v.
                    - template_rect (dict): Template coordinates with x, y,
                                            width, and height values.
        """
        self.frame = frame
        self.template = template

        self.num_particles = kwargs.get('num_particles')  # required by the autograder
        self.sigma_exp = kwargs.get('sigma_exp')  # required by the autograder
        self.sigma_dyn = kwargs.get('sigma_dyn')  # required by the autograder
        self.template_rect = kwargs.get('template_coords')  # required by the autograder
        # If you want to add more parameters, make sure you set a default value so that
        # your test doesn't fail the autograder because of an unknown or None value.
        #
        # The way to do it is:
        # self.some_parameter_name = kwargs.get('parameter_name', default_value)


        # Top Left X, Y Coordinates. Width and Height of Template
        x, y, w, h = self.template_rect['x'], self.template_rect['y'] \
                     , self.template_rect['w'], self.template_rect['h']

        # Initialize your particles array. Read the docstring.
        # Create particles from a Gaussian around the center
        # MDT play with sigma below - make a function of the average of the width and height
        # Be smarter about initialization on don't create particles near the edge
        # Center of the initial frame
        particle_x = x + w // 2
        particle_y = y + h // 2
        # MDT I should have a loop and check each frame size to see if it is valid here first
        # particles_x = np.random.normal(particle_x, self.sigma_dyn / 2,
        #                                size=self.num_particles)
        # particles_y = np.random.normal(particle_y, self.sigma_dyn / 2,
        #                                size=self.num_particles)
        # self.particles = np.column_stack((particles_x, particles_y)).astype(int)

        self.particles = np.zeros((0, 2), dtype=int)
        new_particle_x = 0
        new_particle_y = 0
        #import ipdb; ipdb.set_trace()
        setup = 0
        while self.particles.shape[0] != self.num_particles:
            setup += 1
            # Dynamically reduce sigma_dyn as the number of iterations hits a threshold of 10?
            new_particle_x = int(np.around(np.random.normal(particle_x, self.sigma_dyn / 2)))
            new_particle_y = int(np.around(np.random.normal(particle_y, self.sigma_dyn / 2)))
            particle_frame_cutout = self.get_particle_frame((new_particle_x, new_particle_y), frame)
            if particle_frame_cutout.shape == self.template.shape:
                self.particles = np.vstack((self.particles, [new_particle_x, new_particle_y]))

        #print "Did {} iterations".format(setup)

        # Initialize your weights array. Read the docstring.
        self.weights = np.full((self.num_particles, ),
                               1.0 / self.num_particles).astype(np.float64)

        # Initialize any other components you may need when designing your filter.


    def get_particles(self):
        """Returns the current particles state.

        This method is used by the autograder. Do not modify this function.

        Returns:
            numpy.array: particles data structure.
        """
        return self.particles

    def get_weights(self):
        """Returns the current particle filter's weights.

        This method is used by the autograder. Do not modify this function.

        Returns:
            numpy.array: weights data structure.
        """
        return self.weights

    def get_error_metric(self, template, frame_cutout):
        """Returns the error metric used based on the similarity measure.

        Returns:
            float: similarity value.
        """
        # Convert images to grayscale
        # TODO make sure no rounding or overflow here
        difference = frame_cutout.astype(np.float32) - template.astype(np.float32)
        error = cv2.cvtColor(difference, cv2.COLOR_BGR2GRAY)
        mse = (error ** 2).mean()
        similarity_measure = np.exp(-mse / (2.0 * (self.sigma_exp ** 2)))

        return similarity_measure

    def resample_particles(self):
        """Returns a new set of particles

        This method does not alter self.particles.

        Use self.num_particles and self.weights to return an array of
        resampled particles based on their weights.

        See np.random.choice or np.random.multinomial.

        Returns:
            numpy.array: particles data structure.
        """
        particle_indices = np.random.choice(self.particles.shape[0],
                                            self.num_particles,
                                            p=self.weights)
        new_particles = self.particles[particle_indices, :]

        return new_particles

    def get_particle_frame(self, particle, frame):
        particle_x, particle_y = particle

        # MDT I changed this
        #w, h = self.template_rect['w'], self.template_rect['h']
        h, w = self.template.shape[:2]

        top_left = (int(particle_x - w/2), int(particle_y - h/2))
        frame_roi = frame[top_left[1]:top_left[1] + h, top_left[0]:top_left[0] + w]

        return frame_roi

    def process(self, frame):
        """Processes a video frame (image) and updates the filter's state.

        Implement the particle filter in this method returning None
        (do not include a return call). This function should update the
        particles and weights data structures.

        Make sure your particle filter is able to cover the entire area of the
        image. This means you should address particles that are close to the
        image borders.

        Args:
            frame (numpy.array): color BGR uint8 image of current video frame,
                                 values in [0, 255].

        Returns:
            None.
        """
        new_particles = np.zeros((self.num_particles, 2), dtype=int)
        new_weights = np.zeros(self.num_particles, dtype=np.float64)

        for i, particle in enumerate(self.particles):
            # Update Particles
            # Sample x_i_t from p(x_t | x_t-1, u_t) using x_j(i)_t and u_t -- Control
            # What to do when the particle center falls off of center
            # check shape of new frame and if it doesn't fit then generate a new particle to replace it
            # Use large sigma values when near an edge?
            # Use smaller sigma values when near the center?
            generated_valid_particle = False
            # TODO remove line below
            new_particle = (0, 0)
            while not generated_valid_particle:
                # Dynamically reduce sigma_dyn as the number of iterations hits a threshold of 10?
                new_particle_x = int(np.around(np.random.normal(particle[0], self.sigma_dyn / 2)))
                new_particle_y = int(np.around(np.random.normal(particle[1], self.sigma_dyn / 2)))
                new_particle = (new_particle_x, new_particle_y)
                particle_frame_cutout = self.get_particle_frame(new_particle, frame)
                if particle_frame_cutout.shape == self.template.shape:
                    generated_valid_particle = True
                    # Compute importance to reweight on new frame
                    similarity_measure = self.get_error_metric(self.template, particle_frame_cutout)
                    # Insert new particle and weight
                    new_particles[i] = np.array(new_particle, dtype=int)
                    new_weights[i] = similarity_measure

        # Update sample of particles
        self.particles = new_particles
        # Normalize weights
        self.weights = new_weights / new_weights.sum()

        # Resample Particles from new weights
        self.particles = self.resample_particles()

    def render(self, frame_in):
        # Leveraged code from Christian Stober from Piazza

        # constants - where is the best place to store these? Class level?
        particle_color = (0, 0, 255)
        rect_color = (0, 255, 0)
        circle_color = (0, 255, 0)

        particle_radius = 1 # pels
        rect_thickness = 1
        circle_thickness = 1

        #x_weighted_mean, y_weighted_mean = means = np.dot(self.weights.transpose(),
                                                    #self.particles).astype(np.int)
        x_weighted_mean, y_weighted_mean = means = \
            np.average(self.particles, axis=0, weights=self.weights).astype(np.int)

        # draw the particles - can this be meaningfully vectorized?
        for particle in self.particles:
            cv2.circle(frame_in, tuple(particle), particle_radius, particle_color, -1)

        # draw bounding rectangle
        h, w = self.template.shape[:2]
        #print "{} {}".format(x_weighted_mean, y_weighted_mean)
        top_left = (x_weighted_mean - w / 2, y_weighted_mean - h / 2)
        bottom_right = (top_left[0] + w, top_left[1] + h)
        #print "{} {}".format(top_left, bottom_right)
        cv2.rectangle(frame_in, top_left, bottom_right, rect_color, rect_thickness)

        # draw deviation circle
        d_mean = (((self.particles - means) ** 2).sum(axis = 1) ** 0.5 * self.weights).sum()
        cv2.circle(frame_in, tuple(means), int(d_mean), circle_color, circle_thickness)


class AppearanceModelPF(ParticleFilter):
    """A variation of particle filter tracker."""

    def __init__(self, frame, template, **kwargs):
        """Initializes the appearance model particle filter.

        The documentation for this class is the same as the ParticleFilter
        above. There is one element that is added called alpha which is
        explained in the problem set documentation. By calling super(...) all
        the elements used in ParticleFilter will be inherited so you do not
        have to declare them again.
        """

        super(AppearanceModelPF, self).__init__(frame, template, **kwargs)  # call base class constructor

        self.alpha = kwargs.get('alpha')  # required by the autograder
        # If you want to add more parameters, make sure you set a default value so that
        # your test doesn't fail the autograder because of an unknown or None value.
        #
        # The way to do it is:
        # self.some_parameter_name = kwargs.get('parameter_name', default_value)

    def process(self, frame):
        """Processes a video frame (image) and updates the filter's state.

        This process is also inherited from ParticleFilter. Depending on your
        implementation, you may comment out this function and use helper
        methods that implement the "Appearance Model" procedure.

        Args:
            frame (numpy.array): color BGR uint8 image of current video frame, values in [0, 255].

        Returns:
            None.
        """
        new_particles = np.zeros((self.num_particles, 2), dtype=int)
        new_weights = np.zeros(self.num_particles, dtype=np.float64)

        for i, particle in enumerate(self.particles):
            # Update Particles
            # Sample x_i_t from p(x_t | x_t-1, u_t) using x_j(i)_t and u_t -- Control
            # What to do when the particle center falls off of center
            # check shape of new frame and if it doesn't fit then generate a new particle to replace it
            # Use large sigma values when near an edge?
            # Use smaller sigma values when near the center?
            generated_valid_particle = False
            # TODO remove line below
            new_particle = (0, 0)
            while not generated_valid_particle:
                # Dynamically reduce sigma_dyn as the number of iterations hits a threshold of 10?
                new_particle_x = int(np.around(np.random.normal(particle[0], self.sigma_dyn / 2)))
                new_particle_y = int(np.around(np.random.normal(particle[1], self.sigma_dyn)))
                new_particle = (new_particle_x, new_particle_y)
                particle_frame_cutout = self.get_particle_frame(new_particle, frame)
                if particle_frame_cutout.shape == self.template.shape:
                    generated_valid_particle = True
                    # Compute importance to reweight on new frame
                    similarity_measure = self.get_error_metric(self.template, particle_frame_cutout)
                    # Insert new particle and weight
                    new_particles[i] = np.array(new_particle, dtype=int)
                    new_weights[i] = similarity_measure

        # Update sample of particles
        self.particles = new_particles
        # Normalize weights
        self.weights = new_weights / new_weights.sum()

        # Resample Particles from new weights
        self.particles = self.resample_particles()

        # Update Template
        # x_weighted_mean, y_weighted_mean = np.average(self.particles,
        #                                               axis=0,
        #                                               weights=self.weights
        #                                               ).astype(np.int)
        max_weight_idx = self.weights.argmax()
        x_weighted_mean, y_weighted_mean = self.particles[max_weight_idx, :]
        #h, w = self.template.shape[:2]
        best = self.get_particle_frame((x_weighted_mean, y_weighted_mean), frame)
        # TODO MDT This is wrong use: cv2.addWeighted()
        # self.template = (self.alpha * best) + (1.0 - self.alpha) * self.template
        self.template = cv2.addWeighted(best, self.alpha, self.template, (1.0 - self.alpha), 0.0)


class MDParticleFilter(AppearanceModelPF):
    """A variation of particle filter tracker that incorporates more dynamics."""

    def __init__(self, frame, template, **kwargs):
        """Initializes MD particle filter object.

        The documentation for this class is the same as the ParticleFilter
        above. By calling super(...) all the elements used in ParticleFilter
        will be inherited so you don't have to declare them again.
        """

        super(MDParticleFilter, self).__init__(frame, template, **kwargs)  # call base class constructor
        # If you want to add more parameters, make sure you set a default value so that
        # your test doesn't fail the autograder because of an unknown or None value.
        #
        # The way to do it is:
        # self.some_parameter_name = kwargs.get('parameter_name', default_value)
        self.sigma_scale = kwargs.get('sigma_scale', self.sigma_dyn)
        self.template_reducing = kwargs.get('template_reducing', False)

        self.original_template = np.copy(self.template)
        self.original_sigma_dyn = self.sigma_dyn
        self.original_sigma_scale = self.sigma_scale
        self.occluded = False

        # Augment particles with size of template
        # Assuming that we are decreasing in size
        #import ipdb; ipdb.set_trace()
        # particles_template_size = 1.0 - np.absolute(
        #                               np.random.normal(0.0, self.sigma_scale/1000,
        #                               size=self.num_particles+1800)
        #                           ).astype(np.float64)
        # particles_template_size2 = 1.0 - np.absolute(
        #                               np.random.normal(0.0, self.sigma_scale*1000,
        #                               size=self.num_particles+1800)
        #                           ).astype(np.float64)
        # particles_template_size = np.random.normal(0.0, self.sigma_scale/100.0,
        #                               size=self.num_particles+1800)

        if self.template_reducing:
            particles_template_scale = np.random.normal(0.0, self.sigma_scale,
                                                       size=self.num_particles)
            particles_template_scale = 1.0 - np.absolute(particles_template_scale)
        else:
            particles_template_scale = np.random.normal(1.0, self.sigma_scale,
                                                       size=self.num_particles)

        # GRAPH TESTING
        # new_data = particles_template_scale
        # density = stats.kde.gaussian_kde(new_data)
        # xs = np.linspace(-1.5,1.5,2000)
        # density.covariance_factor = lambda : .05
        # density._compute_covariance()
        # plt.plot(xs, density(xs),color='green')
        # plt.show()
        # exit()
        # END GRAPH TESTING

        self.particles = np.column_stack((self.particles, particles_template_scale)
                                        ).astype(np.float64)


        # cv2.imshow('original', self.template)
        # cv2.waitKey(0)
        # self.template = cv2.resize(self.original_template,
        #                               None,
        #                               fx=0.5,
        #                               fy=0.5
        #                           )
        # cv2.imshow('fifty percent', self.template)
        # cv2.waitKey(0)
        # self.template = cv2.resize(self.original_template,
        #                               None,
        #                               fx=0.98,
        #                               fy=0.98
        #                           )
        # cv2.imshow('98 percent', self.template)
        # cv2.waitKey(0)
        # exit()

    def get_particle_frame_new(self, particle, frame, template):
        # Unpacking three values now
        particle_x, particle_y, _ = particle
        h, w = template.shape[:2]

        top_left = (int(particle_x - w/2), int(particle_y - h/2))
        frame_roi = frame[top_left[1]:top_left[1] + h, top_left[0]:top_left[0] + w]

        return frame_roi

    def process(self, frame):
        """Processes a video frame (image) and updates the filter's state.

        This process is also inherited from ParticleFilter. Depending on your
        implementation, you may comment out this function and use helper
        methods that implement the "More Dynamics" procedure.

        Args:
            frame (numpy.array): color BGR uint8 image of current video frame,
                                 values in [0, 255].

        Returns:
            None.
        """
        new_particles = np.zeros((self.num_particles, 3), dtype=np.float64)
        new_weights = np.zeros(self.num_particles, dtype=np.float64)

        for i, particle in enumerate(self.particles):
            # Update Particles
            # Sample x_i_t from p(x_t | x_t-1, u_t) using x_j(i)_t and u_t -- Control
            # What to do when the particle center falls off of center
            # check shape of new frame and if it doesn't fit then generate a new particle to replace it
            # Use large sigma values when near an edge?
            # Use smaller sigma values when near the center?
            generated_valid_particle = False
            while not generated_valid_particle:
                # Dynamically reduce sigma_dyn as the number of iterations hits a threshold of 10?

                new_particle_x = int(np.around(np.random.normal(particle[0], self.sigma_dyn / 6.5)))
                new_particle_y = int(np.around(np.random.normal(particle[1], self.sigma_dyn / 3.5)))

                #import ipdb; ipdb.set_trace()
                # if self.template_reducing:
                #     new_template_scale = np.random.normal(particle[2], self.sigma_scale)
                #     new_template_scale = particle[2] - np.absolute(
                #                                            particle[2] - new_template_scale
                #                                        ).astype(np.float64)
                #     # Interpolation method for shrinking
                #     resize_interpolation = cv2.INTER_AREA
                # else:
                new_template_scale = np.random.normal(1.0, self.sigma_scale*1.5)
                #new_template_scale = np.random.normal(particle[2], self.sigma_scale)
                # Interpolation method for both shrinking and zooming
                resize_interpolation = cv2.INTER_LINEAR

                new_particle = (new_particle_x, new_particle_y, new_template_scale)

                # Resize the original template
                # MDT use self.template instead of original
                new_template = cv2.resize(self.template,
                                          None,
                                          fx=new_template_scale,
                                          fy=new_template_scale,
                                          interpolation=resize_interpolation)

                particle_frame_cutout = self.get_particle_frame_new(new_particle,
                                                                    frame,
                                                                    new_template)

                if particle_frame_cutout.shape == new_template.shape:
                    generated_valid_particle = True
                    # Compute importance to reweight on new frame
                    similarity_measure = self.get_error_metric(new_template, particle_frame_cutout)

                    # Insert new particle and weight
                    new_particles[i] = np.array(new_particle, dtype=np.float64)
                    new_weights[i] = similarity_measure

        # Update sample of particles
        # self.particles = new_particles
        # Normalize weights
        # self.weights = new_weights / new_weights.sum()

        # Resample Particles from new weights
        # self.particles = self.resample_particles()

        new_weights = new_weights / new_weights.sum()
        sampled_particles = self.resample_particles_new(new_particles, new_weights)

        # Alternative approach!
        # Look at STD of sampled_particles and threshold it


        # Update Template
        # max_weight_idx = self.weights.argmax()
        # x_weighted_mean, y_weighted_mean = self.particles[max_weight_idx, :]
        # h, w = self.template.shape[:2]
        # best = self.get_particle_frame((x_weighted_mean, y_weighted_mean), frame)
        # self.template = (self.alpha * best) + (1.0 - self.alpha) * self.template

        # Naive approach of just downscaling
        # If the change from the previous highest rank is a lot then don't
        # update particles and simply and only reduce the size of the template

        # import ipdb; ipdb.set_trace()
        # Need to update original template too
        max_weight_idx = new_weights.argmax()
        x_weighted_mean, y_weighted_mean, template_scale = sampled_particles[
                                                               max_weight_idx, :
                                                           ]
        resize_interpolation = cv2.INTER_LINEAR
        # Take the previous best template and make it the same size
        max_template = cv2.resize(self.template,
                                  None,
                                  fx=template_scale,
                                  fy=template_scale,
                                  interpolation=resize_interpolation
                                 )
        # cv2.imshow('Template Before', self.template)
        # cv2.waitKey(0)
        best = self.get_particle_frame_new((x_weighted_mean, y_weighted_mean, None), frame, max_template)
        # cv2.imshow('Template Max Template', max_template)
        # cv2.waitKey(0)

        # cv2.imshow('Template Best', best)
        # cv2.waitKey(1)

        best_rows, best_cols = best.shape[:2]
        original_resized = cv2.resize(self.original_template,
                                      (best_cols, best_rows),
                                      interpolation=resize_interpolation
                                     )
        # cv2.imshow('Original Template Best', original_resized)
        # cv2.waitKey(1)
        difference = 0.0
        # difference = self.get_error_metric(original_resized, best)
        # difference = self.get_error_metric(max_template, best) + difference
        # print "Difference: {}".format(difference)
        std = np.std(sampled_particles[:,:2][:,0]) + np.std(sampled_particles[:,:2][:,1])
        # print "STD: {}".format()
        # print "Alpha is: {}".format(self.alpha)

        if std < 4.45:
        # if True or (difference > 1.0e-7) or (self.occluded and difference > 1.0e-7):
            self.template = cv2.addWeighted(best, self.alpha, max_template, (1.0 - self.alpha), 0.0)
            cv2.imshow('Template After', self.template)
            cv2.waitKey(1)

            # Update sample of particles
            self.particles = sampled_particles
            # Normalize weights
            self.weights = new_weights

            # Resample Particles from new weights
            self.particles = self.resample_particles()
            # Reset the motion sigmas
            # self.sigma_dyn = self.original_sigma_dyn
            # self.sigma_scale = self.original_sigma_scale
            self.occluded = False
        else:
            # Diffuse the particles but do not update their weights
            self.particles = new_particles
            # self.weights = new_weights
            # self.sigma_dyn /= 1.05
            # self.sigma_scale /= 1.05
            self.occluded = True
            pass



    def render(self, frame_in):
        # Leveraged code from Christian Stober from Piazza

        # constants - where is the best place to store these? Class level?
        particle_color = (0, 0, 255)
        rect_color = (0, 255, 0)
        circle_color = (0, 255, 0)

        particle_radius = 1 # pels
        rect_thickness = 1
        circle_thickness = 1

        # Merge line below with parent class impl
        # import ipdb; ipdb.set_trace()
        particles = np.around(self.particles[:,:2]).astype(int)

        #x_weighted_mean, y_weighted_mean = means = np.dot(self.weights.transpose(),
                                                    #self.particles).astype(np.int)
        x_weighted_mean, y_weighted_mean = means = \
            np.average(particles, axis=0, weights=self.weights).astype(np.int)

        # draw the particles - can this be meaningfully vectorized?
        for particle in particles:
            cv2.circle(frame_in, tuple(particle), particle_radius, particle_color, -1)

        # draw bounding rectangle
        h, w = self.template.shape[:2]
        #print "{} {}".format(x_weighted_mean, y_weighted_mean)
        top_left = (x_weighted_mean - w / 2, y_weighted_mean - h / 2)
        bottom_right = (top_left[0] + w, top_left[1] + h)
        #print "{} {}".format(top_left, bottom_right)
        cv2.rectangle(frame_in, top_left, bottom_right, rect_color, rect_thickness)

        # draw deviation circle
        d_mean = (((particles - means) ** 2).sum(axis = 1) ** 0.5 * self.weights).sum()
        cv2.circle(frame_in, tuple(means), int(d_mean), circle_color, circle_thickness)

    def render_template(self):
        return self.template

    def resample_particles_new(self, particles, weights):
        particle_indices = np.random.choice(particles.shape[0],
                                            self.num_particles,
                                            p=weights)
        new_particles = particles[particle_indices, :]

        return new_particles


class MDParticleFilterPart5(AppearanceModelPF):
    """A variation of particle filter tracker that incorporates more dynamics."""

    def __init__(self, frame, template, **kwargs):
        """Initializes MD particle filter object.

        The documentation for this class is the same as the ParticleFilter
        above. By calling super(...) all the elements used in ParticleFilter
        will be inherited so you don't have to declare them again.
        """

        super(MDParticleFilterPart5, self).__init__(frame, template, **kwargs)  # call base class constructor
        self.sigma_scale = kwargs.get('sigma_scale')
        self.avg_velocity_x = kwargs.get('avg_velocity_x')
        self.sigma_velocity = kwargs.get('sigma_velocity')
        self.sigma_dyn_x = kwargs.get('sigma_dyn_x')
        self.sigma_dyn_y = kwargs.get('sigma_dyn_y')

        self.original_template = np.copy(self.template)
        self.original_sigma_dyn = self.sigma_dyn
        self.original_sigma_scale = self.sigma_scale
        self.occluded = False
        self.debug = kwargs.get('debug', False)
        self.std_occlusion = kwargs.get('std_occlusion', 20.0)

        particles_template_scale = np.random.normal(1.0, self.sigma_scale,
                                                   size=self.num_particles)

        particles_velocity_x = np.random.normal(self.avg_velocity_x, self.sigma_velocity,
                                                   size=self.num_particles)
        self.particles = np.column_stack((self.particles, particles_template_scale, particles_velocity_x)
                                        ).astype(np.float64)


    def get_particle_frame_new(self, particle, frame, template):
        # Unpacking three values now
        particle_x, particle_y, _, _ = particle
        h, w = template.shape[:2]

        top_left = (int(particle_x - w/2), int(particle_y - h/2))
        frame_roi = frame[top_left[1]:top_left[1] + h, top_left[0]:top_left[0] + w]

        return frame_roi

    def process(self, frame):
        """Processes a video frame (image) and updates the filter's state.

        This process is also inherited from ParticleFilter. Depending on your
        implementation, you may comment out this function and use helper
        methods that implement the "More Dynamics" procedure.

        Args:
            frame (numpy.array): color BGR uint8 image of current video frame,
                                 values in [0, 255].

        Returns:
            None.
        """
        new_particles = np.zeros((self.num_particles, 4), dtype=np.float64)
        new_weights = np.zeros(self.num_particles, dtype=np.float64)

        for i, particle in enumerate(self.particles):
            # Update Particles
            # Sample x_i_t from p(x_t | x_t-1, u_t) using x_j(i)_t and u_t -- Control
            # What to do when the particle center falls off of center
            # check shape of new frame and if it doesn't fit then generate a new particle to replace it
            # Use large sigma values when near an edge?
            # Use smaller sigma values when near the center?
            generated_valid_particle = False
            while not generated_valid_particle:
                # Dynamically reduce sigma_dyn as the number of iterations hits a threshold of 10?

                new_particle_v_x = int(np.around(np.random.normal(particle[3], self.sigma_velocity)))
                # if self.debug:
                #     print "V:{}".format(new_particle_v_x)

                new_particle_x = int(np.around(np.random.normal(particle[0] + new_particle_v_x, self.sigma_dyn_x)))
                new_particle_y = int(np.around(np.random.normal(particle[1], self.sigma_dyn_y)))

                #import ipdb; ipdb.set_trace()
                # if self.template_reducing:
                #     new_template_scale = np.random.normal(particle[2], self.sigma_scale)
                #     new_template_scale = particle[2] - np.absolute(
                #                                            particle[2] - new_template_scale
                #                                        ).astype(np.float64)
                #     # Interpolation method for shrinking
                #     resize_interpolation = cv2.INTER_AREA
                # else:
                new_template_scale = np.random.normal(1.0, self.sigma_scale*1.5)
                #new_template_scale = np.random.normal(particle[2], self.sigma_scale)
                # Interpolation method for both shrinking and zooming
                resize_interpolation = cv2.INTER_LINEAR

                new_particle = (new_particle_x, new_particle_y, new_template_scale, new_particle_v_x)

                # Resize the template
                new_template = cv2.resize(self.template,
                                          None,
                                          fx=new_template_scale,
                                          fy=new_template_scale,
                                          interpolation=resize_interpolation)

                particle_frame_cutout = self.get_particle_frame_new(new_particle,
                                                                    frame,
                                                                    new_template)

                if particle_frame_cutout.shape == new_template.shape:
                    generated_valid_particle = True
                    # Compute importance to reweight on new frame
                    similarity_measure = self.get_error_metric(new_template, particle_frame_cutout)

                    # Insert new particle and weight
                    new_particles[i] = np.array(new_particle, dtype=np.float64)
                    new_weights[i] = similarity_measure
                else:
                    generated_valid_particle = True
                    # print "Fail: {} {}".format(particle_frame_cutout.shape, new_template.shape)

        # Update sample of particles
        # self.particles = new_particles
        # Normalize weights
        # self.weights = new_weights / new_weights.sum()

        # Resample Particles from new weights
        # self.particles = self.resample_particles()

        new_weights = new_weights / new_weights.sum()
        sampled_particles = self.resample_particles_new(new_particles, new_weights)

        # Alternative approach!
        # Look at STD of sampled_particles and threshold it


        # Update Template
        # max_weight_idx = self.weights.argmax()
        # x_weighted_mean, y_weighted_mean = self.particles[max_weight_idx, :]
        # h, w = self.template.shape[:2]
        # best = self.get_particle_frame((x_weighted_mean, y_weighted_mean), frame)
        # self.template = (self.alpha * best) + (1.0 - self.alpha) * self.template

        # Naive approach of just downscaling
        # If the change from the previous highest rank is a lot then don't
        # update particles and simply and only reduce the size of the template

        # import ipdb; ipdb.set_trace()
        # Need to update original template too
        max_weight_idx = new_weights.argmax()
        x_weighted_mean, y_weighted_mean, template_scale, _ = sampled_particles[
                                                               max_weight_idx, :
                                                           ]
        resize_interpolation = cv2.INTER_LINEAR
        # Take the previous best template and make it the same size
        max_template = cv2.resize(self.template,
                                  None,
                                  fx=template_scale,
                                  fy=template_scale,
                                  interpolation=resize_interpolation
                                 )
        # cv2.imshow('Template Before', self.template)
        # cv2.waitKey(0)
        best = self.get_particle_frame_new((x_weighted_mean, y_weighted_mean, None, None), frame, max_template)
        # cv2.imshow('Template Max Template', max_template)
        # cv2.waitKey(1)

        # cv2.imshow('Template Best', best)
        # cv2.waitKey(1)

        best_rows, best_cols = best.shape[:2]
        original_resized = cv2.resize(self.original_template,
                                      (best_cols, best_rows),
                                      interpolation=resize_interpolation
                                     )
        # cv2.imshow('Original Template Best', original_resized)
        # cv2.waitKey(1)
        difference = 0.0
        # difference = self.get_error_metric(original_resized, best)
        # difference = self.get_error_metric(max_template, best) + difference
        # print "Difference: {}".format(difference)
        std = np.std(sampled_particles[:,:2][:,0]) + np.std(sampled_particles[:,:2][:,1])
        if self.debug:
            print "STD: {}".format(std)
        # print "Alpha is: {}".format(self.alpha)

        if std < self.std_occlusion:
        # if True or (difference > 1.0e-7) or (self.occluded and difference > 1.0e-7):
            self.template = cv2.addWeighted(best, self.alpha, max_template, (1.0 - self.alpha), 0.0)
            if self.debug:
                cv2.imshow('Template After', self.template)
                cv2.waitKey(1)

            # Update sample of particles
            self.particles = sampled_particles
            # Normalize weights
            self.weights = new_weights

            # Resample Particles from new weights
            self.particles = self.resample_particles()
            # Reset the motion sigmas
            # self.sigma_dyn = self.original_sigma_dyn
            # self.sigma_scale = self.original_sigma_scale
            self.occluded = False
        else:
            # Diffuse the particles but do not update their weights
            self.particles = new_particles
            self.weights = new_weights
            # self.sigma_dyn /= 1.05
            # self.sigma_scale /= 1.05
            self.occluded = True
            pass


    def render(self, frame_in):
        # Leveraged code from Christian Stober from Piazza

        # constants - where is the best place to store these? Class level?
        particle_color = (0, 0, 255)
        rect_color = (0, 255, 0)
        circle_color = (0, 255, 0)

        particle_radius = 1 # pels
        rect_thickness = 1
        circle_thickness = 1

        # Merge line below with parent class impl
        # import ipdb; ipdb.set_trace()
        particles = np.around(self.particles[:,:2]).astype(int)

        #x_weighted_mean, y_weighted_mean = means = np.dot(self.weights.transpose(),
                                                    #self.particles).astype(np.int)
        x_weighted_mean, y_weighted_mean = means = \
            np.average(particles, axis=0, weights=self.weights).astype(np.int)

        # draw the particles - can this be meaningfully vectorized?
        for particle in particles:
            cv2.circle(frame_in, tuple(particle), particle_radius, particle_color, -1)

        # draw bounding rectangle
        h, w = self.template.shape[:2]
        #print "{} {}".format(x_weighted_mean, y_weighted_mean)
        top_left = (x_weighted_mean - w / 2, y_weighted_mean - h / 2)
        bottom_right = (top_left[0] + w, top_left[1] + h)
        #print "{} {}".format(top_left, bottom_right)
        cv2.rectangle(frame_in, top_left, bottom_right, rect_color, rect_thickness)

        # draw deviation circle
        d_mean = (((particles - means) ** 2).sum(axis = 1) ** 0.5 * self.weights).sum()
        cv2.circle(frame_in, tuple(means), int(d_mean), circle_color, circle_thickness)

    def render_template(self):
        return self.template

    def resample_particles_new(self, particles, weights):
        particle_indices = np.random.choice(particles.shape[0],
                                            self.num_particles,
                                            p=weights)
        new_particles = particles[particle_indices, :]

        return new_particles
